### Route your browser's traffic through Fizzgun

A simple automatic way to feed Fizzgun with request samples is to [use existing integration tests](using-sut-tests.md),
but if they are not available and you system under test happens to have a UI, you can just configure your browser to
route traffic through Fizzgun and manually navigate your application.

For an easy setup I recommend using Firefox, since it doesn't require you to change the system global proxy settings
(otherwise all of your HTTP traffic will be going through Fizzgun). 

**Firefox**: 

 * Go to `Preferences -> Advanced -> Network -> Settings..` (this may vary depending your OS and Firefox version)
 * Select `Manual proxy configuration`
    - set `HTTP Proxy` to `127.0.0.1`
    - and `Port` to `8888`
    - tick `Use this proxy server for all protocols`.
 * If the SUT is running on localhost, also remove the entries under `No Proxy for`

**Chrome**:

To avoid changing the system global proxy settings, you can install an add-on such as
[Proxy Switcher](https://chrome.google.com/webstore/detail/proxy-switcher-manager/onnfghpihccifgojkpnnncpagjcdbjod?hl=en)
