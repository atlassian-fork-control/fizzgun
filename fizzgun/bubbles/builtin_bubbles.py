from fizzgun.bubbles.data_validation import Enlarger, Trimmer, TypeChanger
from fizzgun.bubbles.security import Injector, Shellshock

BUBBLES = [Enlarger, Trimmer, TypeChanger, Injector, Shellshock]
