import time

import pytest
import requests

from tests.e2e.support.process_wrapper import ProcessWrapper
from tests.e2e.support import target_web_app


def is_http_server_ready(port):
    try:
        requests.get('http://127.0.0.1:%d/' % port)
        return True
    except:
        return False


def wait_for(func, timeout=1, step=0.1, initial_wait=0):
    time.sleep(initial_wait)
    start = time.time()
    while not func() and time.time() < start + timeout:
        time.sleep(step)


class WebApp(object):
    def __init__(self, port):
        self._port = port

    @property
    def port(self):
        return self._port

    @property
    def base_url(self):
        return 'http://127.0.0.1:%d' % self.port

    def wait_until_ready(self):
        return wait_for(lambda: is_http_server_ready(self.port), timeout=5)

    def will_return_response(self, status=200, headers=None, body=None):
        response = {
            'status': status,
            'headers': headers,
            'body': body
        }
        requests.post(self.base_url + '/configure', json=response)


@pytest.fixture()
def web_app():
    web_app_info = WebApp(4567)

    with ProcessWrapper(target_web_app.run, web_app_info.port, on_enter=web_app_info.wait_until_ready):
        yield web_app_info
