import copy
from flask import Flask, request, Response

app = Flask(__name__)


class ResponseBuilder(object):
    def __init__(self, status=200, headers=None, body=None):
        self._status = status
        self._headers = copy.deepcopy(headers or {})
        self._body = body

    def with_status(self, status):
        return ResponseBuilder(status, self._headers, self._body)

    def with_headers(self, headers):
        return ResponseBuilder(self._status, headers, self._body)

    def with_body(self, body):
        return ResponseBuilder(self._status, self._headers, body)

    def build(self):
        return Response(response=self._body, status=self._status, headers=self._headers)


response_builder = ResponseBuilder()


@app.route('/configure', methods=['POST'])
def configure_response():
    state = request.get_json()
    global response_builder
    response_builder = response_builder\
        .with_status(state.get('status'))\
        .with_body(state.get('body'))\
        .with_headers(state.get('headers'))
    return 'ok'


@app.route('/', defaults={'path': ''}, methods=['GET', 'HEAD', 'PUT', 'POST', 'OPTIONS', 'DELETE', 'TRACE'])
@app.route('/<path:path>', methods=['GET', 'HEAD', 'PUT', 'POST', 'OPTIONS', 'DELETE', 'TRACE'])
def catch_all(path):
    return response_builder.build()


def run(port, host='127.0.0.1'):
    app.run(host=host, port=port, threaded=True)
