import pytest

from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.scenarios.bubble_scenario_builder import BubbleScenarioBuilder
from tests.unit.support.fixtures.requests import a_request_with_query, a_json_request, a_www_form_urlencoded_request

enlarger_scenario = BubbleScenarioBuilder('fizzgun.bubbles', 'Enlarger', 'name:enlarger')


trimmer_scenario = BubbleScenarioBuilder('fizzgun.bubbles', 'Trimmer', 'name:trimmer')


type_changer_scenario = BubbleScenarioBuilder('fizzgun.bubbles', 'TypeChanger', 'name:type-changer')


injector_scenario = BubbleScenarioBuilder('fizzgun.bubbles', 'Injector', 'name:injector')


bubble_with_json_support = pytest.mark.parametrize(
    'bubble_scenario',
    [enlarger_scenario, trimmer_scenario, type_changer_scenario, injector_scenario],
    ids=['enlarger', 'trimmer', 'type-changer', 'injector']
)

bubble_with_urlencoded_support = pytest.mark.parametrize(
    'bubble_scenario',
    [enlarger_scenario, trimmer_scenario, injector_scenario],
    ids=['enlarger', 'trimmer', 'injector']
)

bubble_without_text_plain_support = pytest.mark.parametrize(
    'bubble_scenario',
    [enlarger_scenario, trimmer_scenario, type_changer_scenario, injector_scenario],
    ids=['enlarger', 'trimmer', 'type-changer', 'injector']
)

bubble_with_default_expectations = pytest.mark.parametrize(
    'bubble_scenario',
    [enlarger_scenario, trimmer_scenario, type_changer_scenario, injector_scenario],
    ids=['enlarger', 'trimmer', 'type-changer', 'injector']
)


@bubble_with_default_expectations
def test_should_expect_no_500_responses(bubble_scenario: BubbleScenarioBuilder):
    expectations = (
        bubble_scenario
        .collect_expectations()
        .when_input_request(RequestBuilder.with_query('query=string').with_json_payload({'test': 'me'}).build())
    )
    assert len(expectations) > 0
    assert expectations[0] == [{'args': ['0-499'], 'field': 'status', 'op': 'be_in_ranges'}]


@bubble_with_json_support
def test_should_mutate_json_requests(bubble_scenario: BubbleScenarioBuilder):
    mutations = bubble_scenario.when_input_request(a_json_request())
    assert len(mutations) > 0


@bubble_with_json_support
def test_should_not_mutate_json_when_disabled(bubble_scenario: BubbleScenarioBuilder):
    mutations = (
        bubble_scenario
        .given_bubble_settings(json_params=False)
        .when_input_request(a_json_request())
    )
    assert not mutations


@bubble_with_urlencoded_support
def test_should_mutate_request_with_query(bubble_scenario: BubbleScenarioBuilder):
    mutations = bubble_scenario.when_input_request(a_request_with_query())
    assert len(mutations) > 0


@bubble_with_urlencoded_support
def test_should_mutate_request_with_form_urlencoded_body(bubble_scenario: BubbleScenarioBuilder):
    mutations = bubble_scenario.when_input_request(a_www_form_urlencoded_request())
    assert len(mutations) > 0


@bubble_with_urlencoded_support
def test_should_not_mutate_query_string_when_disabled(bubble_scenario: BubbleScenarioBuilder):
    mutations = (
        bubble_scenario
        .given_bubble_settings(urlencoded_params=False)
        .when_input_request(a_request_with_query())
    )
    assert not mutations


@bubble_with_urlencoded_support
def test_should_not_mutate_urlencoded_body_when_disabled(bubble_scenario: BubbleScenarioBuilder):
    mutations = (
        bubble_scenario
        .given_bubble_settings(urlencoded_params=False)
        .when_input_request(a_www_form_urlencoded_request())
    )
    assert not mutations


@bubble_without_text_plain_support
def test_should_not_mutate_other_type_of_request(bubble_scenario: BubbleScenarioBuilder):
    mutations = bubble_scenario.when_input_request(
        RequestBuilder.with_header('Content-Type', 'text/plain').with_body('foo').build())
    assert not mutations
