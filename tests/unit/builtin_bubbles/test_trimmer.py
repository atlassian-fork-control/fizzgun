import pytest

from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.scenarios.bubble_scenario_builder import BubbleScenarioBuilder
from tests.unit.support.fixtures.parametrize_tests import form_urlencoded_or_query
from tests.unit.support.fixtures.requests import a_json_request


@pytest.fixture()
def trimmer_scenario() -> BubbleScenarioBuilder:
    return BubbleScenarioBuilder('fizzgun.bubbles', 'Trimmer', 'name:trimmer')


@form_urlencoded_or_query
def test_should_remove_urlencoded_parameter(
    trimmer_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = trimmer_scenario.when_input_request(request_with_params(('param', 'value')))

    assert len(mutations) == 1
    assert params_in_mutant(mutations[0]) == {}


@form_urlencoded_or_query
def test_should_remove_urlencoded_parameters_separately(
    trimmer_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = trimmer_scenario.when_input_request(request_with_params(('p1', 'v1'), ('p2', 'v2')))

    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'p2': ['v2']})
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'p1': ['v1']})


@form_urlencoded_or_query
def test_should_remove_duplicated_urlencoded_parameters_all_together(
    trimmer_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = trimmer_scenario.when_input_request(request_with_params(('p1', 'v1'), ('p1', 'v2')))

    assert len(mutations) == 1
    assert params_in_mutant(mutations[0]) == {}


def test_should_not_mutate_empty_arrays_in_json(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request([]))
    assert not mutations


def test_should_remove_items_from_arrays(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request([1]))

    assert len(mutations) == 1
    assert mutations[0].json_body == []


def test_should_remove_items_from_arrays_separately(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request([1, 2]))

    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if mutation.json_body == [2])
    assert any(mutation for mutation in mutations if mutation.json_body == [1])


def test_should_remove_items_from_nested_arrays(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request({'key': [1, {'key2': [1, 'bye', 3]}]}))

    assert any(mutation for mutation in mutations if mutation.json_body == {'key': [1, {'key2': [1, 3]}]})


def test_should_not_mutate_empty_objects_in_json(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request({}))
    assert not mutations


def test_should_remove_items_from_objects(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request({'key': 'value'}))

    assert len(mutations) == 1
    assert mutations[0].json_body == {}


def test_should_remove_items_from_objects_separately(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request({'k1': 'v1', 'k2': 'v2'}))

    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if mutation.json_body == {'k1': 'v1'})
    assert any(mutation for mutation in mutations if mutation.json_body == {'k2': 'v2'})


def test_should_remove_items_from_nested_objects(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(a_json_request({'key': [1, {'key2': [1, {'bye': 'bye'}, 3]}]}))

    assert any(mutation for mutation in mutations if mutation.json_body == {'key': [1, {'key2': [1, {}, 3]}]})


def test_trim_json_and_query_when_both_present(trimmer_scenario: BubbleScenarioBuilder):
    mutations = trimmer_scenario.when_input_request(
        RequestBuilder.with_json_payload({'key': 'value'}).with_query_args(('query', 'string')).build()
    )

    assert len(mutations) == 2
    assert any(mutation for mutation in mutations
               if mutation.json_body == {} and mutation.query_args == {'query': ['string']})
    assert any(mutation for mutation in mutations
               if mutation.json_body == {'key': 'value'} and mutation.query_args == {})
