import pytest

from tests.unit.support.builders.scenarios.bubble_scenario_builder import BubbleScenarioBuilder
from tests.unit.support.fixtures.requests import a_json_request
from tests.unit.support.mocks import MockRandomGenerator


@pytest.fixture()
def type_changer_scenario() -> BubbleScenarioBuilder:
    return BubbleScenarioBuilder('fizzgun.bubbles', 'TypeChanger', 'name:type-changer')


atomic_type_change_scenarios = {
    'bool': (True, ['float', 'int', 'list', 'null', 'object', 'string']),
    'float': (1.8, ['bool', 'list', 'null', 'object', 'string']),
    'int': (4, ['bool', 'float', 'list', 'null', 'object', 'string']),
    'list': ([], ['bool', 'float', 'int', 'null', 'object', 'string']),
    'null': (None, ['bool', 'float', 'int', 'list', 'object', 'string']),
    'object': ({}, ['bool', 'float', 'int', 'list', 'null', 'string']),
    'string': ('test-string', ['bool', 'float', 'int', 'list', 'null', 'object']),
}

verify_atomic_type_change = pytest.mark.parametrize(
    'json_payload,expect_choice_called_with',
    [args for args in atomic_type_change_scenarios.values()],
    ids=[scenario_id for scenario_id in atomic_type_change_scenarios.keys()]
)


@verify_atomic_type_change
def test_should_change_type_of_single_item(
    type_changer_scenario: BubbleScenarioBuilder, mock_random_generator: MockRandomGenerator,
    json_payload, expect_choice_called_with
):
    mutations = (
        type_changer_scenario
        .with_mock_random_generator(mock_random_generator)
        .when_input_request(a_json_request(json_payload))
    )

    assert len(mutations) == 1
    mock_random_generator.choice.assert_called_once_with(expect_choice_called_with)


def custom_type_samples():
    return {
        'object': {'an': 'object'},
        'list': ['a', 'list'],
        'null': None,
        'string': 'a_string',
        'int': 14,
        'float': 3.14,
        'bool': True
    }


def expected_default_type_samples():
    return {
        'bool': True,
        'float': 3.1415926,
        'int': 42,
        'list': [1, 'FizzGun', 3],
        'null': None,
        'object': {'fizz': 'gun'},
        'string': 'FiZzGuN',
    }


def verify_changed_sample_value(scenarios):
    return pytest.mark.parametrize(
        'choice_returns,expected_value',
        [args for args in scenarios.items()],
        ids=[scenario_id for scenario_id in scenarios.keys()])


@verify_changed_sample_value(expected_default_type_samples())
def test_should_return_default_known_sample_of_the_changed_type(
    type_changer_scenario: BubbleScenarioBuilder, mock_random_generator: MockRandomGenerator,
    choice_returns, expected_value
):
    mock_random_generator.given_choice_returns(choice_returns)
    mutations = (
        type_changer_scenario
        .with_mock_random_generator(mock_random_generator)
        .when_input_request(a_json_request('a'))
    )

    assert len(mutations) == 1
    assert mutations[0].json_body == expected_value


@verify_changed_sample_value(custom_type_samples())
def test_should_allow_specifying_samples_for_each_type(
    type_changer_scenario: BubbleScenarioBuilder, mock_random_generator: MockRandomGenerator,
    choice_returns, expected_value
):
    mock_random_generator.given_choice_returns(choice_returns)
    mutations = (
        type_changer_scenario
        .with_mock_random_generator(mock_random_generator)
        .given_bubble_settings(type_samples=custom_type_samples())
        .when_input_request(a_json_request('a'))
    )

    assert len(mutations) == 1
    assert mutations[0].json_body == expected_value


def test_should_change_the_type_of_each_element_separately(type_changer_scenario: BubbleScenarioBuilder):
    dummy_type_samples = dict(
        (type_name, 'new-type') for type_name in ['object', 'list', 'null', 'string', 'int', 'float', 'bool']
    )

    input_request = {'key1': [1, True, 2.3, False, {'key2': None}], 'key3': []}

    mutations = (
        type_changer_scenario
        .given_bubble_settings(type_samples=dummy_type_samples)
        .when_input_request(a_json_request(input_request))
    )

    mutations = [mutation.json_body for mutation in mutations]

    assert len(mutations) == 9

    assert 'new-type' in mutations
    assert {'key1': 'new-type', 'key3': []} in mutations
    assert {'key1': ['new-type', True, 2.3, False, {'key2': None}], 'key3': []} in mutations
    assert {'key1': [1, 'new-type', 2.3, False, {'key2': None}], 'key3': []} in mutations
    assert {'key1': [1, True, 'new-type', False, {'key2': None}], 'key3': []} in mutations
    assert {'key1': [1, True, 2.3, 'new-type', {'key2': None}], 'key3': []} in mutations
    assert {'key1': [1, True, 2.3, False, 'new-type'], 'key3': []} in mutations
    assert {'key1': [1, True, 2.3, False, {'key2': 'new-type'}], 'key3': []} in mutations
    assert {'key1': [1, True, 2.3, False, {'key2': None}], 'key3': 'new-type'} in mutations
