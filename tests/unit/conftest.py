import pytest
from tests.unit.support.builders.scenarios import (
    FilterScenarioBuilder, TransformerScenarioBuilder, RequestorScenarioBuilder, AssertorScenarioBuilder,
    ReporterScenarioBuilder, CmdBubblesScenarioBuilder, CmdGenerateConfigScenarioBuilder)


from tests.unit.support.mocks import (
    MockModuleImporter, MockIdGenerator, MockStdoutWriter,
    MockRandomGenerator, MockHttpClient, MockFileSystem, MockFactory)


@pytest.fixture()
def filter_scenario_builder() -> FilterScenarioBuilder:
    return FilterScenarioBuilder()


@pytest.fixture()
def transformer_scenario_builder() -> TransformerScenarioBuilder:
    return TransformerScenarioBuilder()


@pytest.fixture()
def requestor_scenario_builder() -> RequestorScenarioBuilder:
    return RequestorScenarioBuilder()


@pytest.fixture()
def assertor_scenario_builder() -> AssertorScenarioBuilder:
    return AssertorScenarioBuilder()


@pytest.fixture()
def reporter_scenario_builder() -> ReporterScenarioBuilder:
    return ReporterScenarioBuilder()


@pytest.fixture()
def cmd_bubbles_scenario_builder() -> CmdBubblesScenarioBuilder:
    return CmdBubblesScenarioBuilder()


@pytest.fixture()
def cmd_generate_config_scenario_builder() -> CmdGenerateConfigScenarioBuilder:
    return CmdGenerateConfigScenarioBuilder()


@pytest.fixture()
def mock_module_importer() -> MockModuleImporter:
    return MockFactory.create_mock_module_importer()


@pytest.fixture()
def mock_id_generator() -> MockIdGenerator:
    return MockFactory.create_mock_id_generator()


@pytest.fixture()
def mock_random_generator() -> MockRandomGenerator:
    return MockFactory.create_mock_random_generator()


@pytest.fixture()
def mock_stdout_writer() -> MockStdoutWriter:
    return MockFactory.create_mock_stdout_writer()


@pytest.fixture()
def mock_http_client() -> MockHttpClient:
    return MockFactory.create_mock_http_client()


@pytest.fixture()
def mock_file_system() -> MockFileSystem:
    return MockFactory.create_mock_file_system()
