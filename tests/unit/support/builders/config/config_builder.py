import copy

import yaml

from fizzgun.config.defaults import defaults


class ConfigBuilder(object):

    def __init__(self, initial=None):
        self._cfg = initial or defaults()

    def with_bubbles_config(self, bubbles_config) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['bubbles'] = copy.deepcopy(bubbles_config)
        return ConfigBuilder(cfg)

    def build(self):
        return self._copy

    def to_yaml(self):
        return yaml.safe_dump(self.build())

    @property
    def _copy(self):
        return copy.deepcopy(self._cfg)
