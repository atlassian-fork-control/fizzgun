from typing import Dict
import copy

from fizzgun.config.defaults import defaults


class ReportConfigBuilder(object):

    def __init__(self, initial=None):
        self._cfg = initial or defaults()['report']

    def with_report_directory(self, directory: str) -> 'ReportConfigBuilder':
        cfg = self._copy
        cfg['directory'] = directory
        return ReportConfigBuilder(initial=cfg)

    def with_report_format(self, report_format: str) -> 'ReportConfigBuilder':
        cfg = self._copy
        cfg['format'] = report_format
        return ReportConfigBuilder(initial=cfg)

    def build(self) -> Dict:
        return self._copy

    @property
    def _copy(self):
        return copy.deepcopy(self._cfg)
