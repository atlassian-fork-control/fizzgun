import copy
from typing import Dict, SupportsInt, AnyStr


class ResponseBuilder(object):

    def __init__(self):
        self._status = 200
        self._reason = 'OK'
        self._headers = []
        self._body = ''

    def with_status(self, status: SupportsInt) -> 'ResponseBuilder':
        return self._copy_with(status=int(status))

    def with_reason(self, reason: AnyStr) -> 'ResponseBuilder':
        return self._copy_with(reason=reason)

    def with_header(self, name, value) -> 'ResponseBuilder':
        request_builder = self._copy_with()
        request_builder._headers.append((name, value))
        return request_builder

    def with_headers(self, *args) -> 'ResponseBuilder':
        request_builder = self._copy_with()
        request_builder._headers.extend((i[0], i[1]) for i in args)
        return request_builder

    def with_body(self, body: AnyStr) -> 'ResponseBuilder':
        return self._copy_with(body=body)

    def build(self) -> Dict:
        return {
            'status': self._status,
            'reason': self._reason,
            'headers': copy.deepcopy(self._headers),
            'body': self._body
        }

    def _copy_with(self, **kwargs) -> 'ResponseBuilder':
        new = ResponseBuilder()
        new._status = kwargs.get('status', self._status)
        new._reason = kwargs.get('reason', self._reason)
        new._headers = copy.deepcopy(kwargs.get('headers', self._headers))
        new._body = kwargs.get('body', self._body)
        return new
