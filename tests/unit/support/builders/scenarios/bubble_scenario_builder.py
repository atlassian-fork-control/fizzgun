import copy
from typing import List, Dict

from fizzgun.models import HttpRequest
from tests.unit.support.builders.config import BubblesConfigBuilder
from tests.unit.support.builders.scenarios import TransformerScenarioBuilder
from tests.unit.support.mocks import MockRandomGenerator


class BubbleScenarioBuilder(object):

    _collectors = {
        'modified_requests': lambda results: [HttpRequest(result['modified']) for result in results],
        'expectations': lambda results: [result['expectations'] for result in results]
    }

    def __init__(self, bubble_pack, bubble_name, unique_tag):
        self._bubble_module = bubble_pack
        self._bubble_name = bubble_name
        self._unique_tag = unique_tag
        self._mock_random_generator = None
        self._bubble_settings = None
        self._collect = None

    def with_mock_random_generator(self, mock_random_generator: MockRandomGenerator):
        return self._copy_with(mock_random_generator=mock_random_generator)

    def given_bubble_settings(self, **kwargs) -> 'BubbleScenarioBuilder':
        return self._copy_with(bubble_settings=kwargs)

    def collect_mutations(self):
        return self._copy_with(collect='modified_requests')

    def collect_expectations(self):
        return self._copy_with(collect='expectations')

    def when_input_request(self, request) -> List[HttpRequest or Dict]:
        scenario = TransformerScenarioBuilder()
        bubbles_config = BubblesConfigBuilder().with_tags_whitelist(self._unique_tag)

        if self._bubble_settings:
            bubbles_config = bubbles_config.with_bubble_packs(
                {'module': self._bubble_module, 'settings': {self._bubble_name: self._bubble_settings}})

        output = (
            scenario
            .with_mock_random_generator(self._mock_random_generator)
            .with_bubbles_config_builder(bubbles_config)
            .given_incoming_requests(request)
            .when_transformer_is_executed()
        )
        if isinstance(output, Exception):
            raise output

        collect = self._collect or 'modified_requests'

        return self._collectors[collect](output.results)

    def _copy_with(self, **kwargs):
        new = BubbleScenarioBuilder(self._bubble_module, self._bubble_name, self._unique_tag)
        new._bubble_settings = copy.deepcopy(kwargs.get('bubble_settings', self._bubble_settings))
        new._mock_random_generator = kwargs.get('mock_random_generator', self._mock_random_generator)
        new._collect = kwargs.get('collect', self._collect)
        return new
