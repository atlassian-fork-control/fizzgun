import collections

from fizzgun.application.service_contexts import CmdGenerateConfigContext
from fizzgun.bin.cli_commands import CmdGenerateConfig

from tests.unit.support.mocks import MockStdoutWriter, MockFileSystem, MockFactory


CmdOptions = collections.namedtuple('CmdOptions', ['filename', 'defaults'])


class CmdGenerateConfigScenarioBuilder(object):

    def __init__(self):
        self._mock_stdout_writer = None
        self._mock_file_system = None
        self._filename = None
        self._defaults = False

    def with_mock_sdtoud_writer(self, stdout_writer: MockStdoutWriter) -> 'CmdGenerateConfigScenarioBuilder':
        return self._copy_with(mock_stdout_writer=stdout_writer)

    def with_mock_file_system(self, file_system: MockFileSystem) -> 'CmdGenerateConfigScenarioBuilder':
        return self._copy_with(mock_file_system=file_system)

    def given_filename_is_passed(self, file_path: str) -> 'CmdGenerateConfigScenarioBuilder':
        return self._copy_with(filename=file_path)

    def given_defaults_flag_is_set(self) -> 'CmdGenerateConfigScenarioBuilder':
        return self._copy_with(defaults=True)

    def when_cmd_generate_config_is_executed(self):
        mock_file_system = self._mock_file_system or MockFactory.create_mock_file_system()
        mock_stdout_writer = self._mock_stdout_writer or MockFactory.create_mock_stdout_writer()

        context = CmdGenerateConfigContext(file_system=mock_file_system, stdout_writer=mock_stdout_writer)
        CmdGenerateConfig(context).run(CmdOptions(filename=self._filename, defaults=self._defaults))

    def _copy_with(self, **kwargs) -> 'CmdGenerateConfigScenarioBuilder':
        new = CmdGenerateConfigScenarioBuilder()
        new._mock_stdout_writer = kwargs.get('mock_stdout_writer', self._mock_stdout_writer)
        new._mock_file_system = kwargs.get('mock_file_system', self._mock_file_system)
        new._filename = kwargs.get('filename', self._filename)
        new._defaults = kwargs.get('defaults', self._defaults)
        return new
