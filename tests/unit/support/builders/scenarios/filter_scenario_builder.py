from typing import List
import copy

from fizzgun.application.service_contexts import FilterContext
from fizzgun.core import Filter
from tests.unit.support.mocks import MockStdoutWriter, MockFactory
from tests.unit.support.mocks.mock_connectors import MockInput, MockOutput


class FilterScenarioBuilder(object):

    def __init__(self):
        self._filters_config = None
        self._mock_stdout_writer = None
        self._incoming_requests = []

    def with_mock_sdtoud_writer(self, stdout_writer: MockStdoutWriter) -> 'FilterScenarioBuilder':
        return self._copy_with(mock_stdout_writer=stdout_writer)

    def with_filters_config(self, filters_config: List) -> 'FilterScenarioBuilder':
        return self._copy_with(filters_config=filters_config)

    def given_incoming_requests(self, *incoming_requests) -> 'FilterScenarioBuilder':
        return self._copy_with(incoming_requests=list(incoming_requests))

    def when_filter_is_executed(self) -> MockOutput or Exception:
        mock_stdout_writer = self._mock_stdout_writer or MockFactory.create_mock_stdout_writer()

        filter_context = FilterContext(stdout_writer=mock_stdout_writer)

        inputq = MockInput([{'original': copy.deepcopy(request)} for request in self._incoming_requests])
        outputq = MockOutput()

        Filter(filter_context, inputq, outputq, filters_config=self._filters_config).start()

        return outputq

    def _copy_with(self, **kwargs) -> 'FilterScenarioBuilder':
        new = FilterScenarioBuilder()
        new._filters_config = copy.deepcopy(kwargs.get('filters_config', self._filters_config))
        new._mock_stdout_writer = kwargs.get('mock_stdout_writer', self._mock_stdout_writer)
        new._incoming_requests = copy.deepcopy(kwargs.get('incoming_requests', self._incoming_requests))
        return new
