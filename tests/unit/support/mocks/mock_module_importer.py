import types
from typing import Tuple, Type
import unittest.mock

import fizzgun.bubbles
from fizzgun.bubbles import Bubble
from fizzgun.application.dependencies import ModuleImporter


class MockModuleImporter(ModuleImporter):
    def given_module_exists(self, name: str, mod: Type[types.ModuleType]):
        pass

    def given_module_with_missing_bubble_attribute_exists(self, module_name: str):
        pass

    def given_module_with_bubbles_exists(self, module_name: str, *bubbles: Tuple[Type[Bubble], ...]):
        pass

    def given_import_modules_raises_error(self, error: Exception):
        pass


def create_mock_module_importer() -> MockModuleImporter:
    mock_module_importer = unittest.mock.create_autospec(spec=ModuleImporter, instance=True)
    modules = {}

    def given_module_exists(name, mod):
        modules[name] = mod

    def given_module_with_missing_bubble_attribute_exists(module_name):
        given_module_exists(module_name, types.ModuleType(module_name))

    def given_module_with_bubbles_exists(module_name, *bubbles):
        given_module_with_missing_bubble_attribute_exists(module_name)
        setattr(modules[module_name], 'BUBBLES', list(bubbles))

    def given_import_modules_raises_error(error: Exception):
        mock_module_importer.import_module.side_effect = error

    setattr(
        mock_module_importer,
        MockModuleImporter.given_module_exists.__name__,
        given_module_exists
    )

    setattr(
        mock_module_importer,
        MockModuleImporter.given_module_with_missing_bubble_attribute_exists.__name__,
        given_module_with_missing_bubble_attribute_exists
    )

    setattr(
        mock_module_importer,
        MockModuleImporter.given_module_with_bubbles_exists.__name__,
        given_module_with_bubbles_exists
    )

    setattr(
        mock_module_importer,
        MockModuleImporter.given_import_modules_raises_error.__name__,
        given_import_modules_raises_error
    )

    mock_module_importer.import_module.side_effect = lambda module_name: modules[module_name]
    mock_module_importer.given_module_exists('fizzgun.bubbles', fizzgun.bubbles)

    return mock_module_importer
