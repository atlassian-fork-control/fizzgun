import random
import unittest.mock

from fizzgun.application.dependencies import RandomGenerator


class MockRandomGenerator(RandomGenerator):
    def given_choice_returns(self, choice):
        pass

    def given_choice_returns_random_value(self):
        pass


def create_mock_random_generator() -> MockRandomGenerator:
    mock_random_generator = unittest.mock.create_autospec(spec=RandomGenerator, instance=True)

    def given_choice_returns(choice: str):
        mock_random_generator.choice.side_effect = lambda _: choice

    def given_choice_returns_random_value():
        mock_random_generator.choice.side_effect = lambda seq: random.choice(seq)

    setattr(mock_random_generator,
            MockRandomGenerator.given_choice_returns.__name__, given_choice_returns)
    setattr(mock_random_generator,
            MockRandomGenerator.given_choice_returns_random_value.__name__, given_choice_returns_random_value)

    mock_random_generator.given_choice_returns_random_value()

    return mock_random_generator
