import re


def Any(cls):
    class Any(cls):
        def __eq__(self, other):
            return isinstance(other, cls)

        def __ne__(self, other):
            return not self == other

        def __str__(self):
            return 'Any(%s)' % cls.__name__

        def __repr__(self):
            return str(self)

    return Any()


def StringMatching(pattern: str):
    pat = re.compile(pattern)

    class StringMatching(str):
        def __eq__(self, other):
            return isinstance(other, str) and pat.search(other) is not None

        def __ne__(self, other):
            return not self == other

        def __str__(self):
            return 'StringMatching(%s)' % pattern

        def __repr__(self):
            return str(self)

    return StringMatching()


def StringContaining(substr: str):
    class StringContaining(str):
        def __eq__(self, other):
            return isinstance(other, str) and substr in other

        def __ne__(self, other):
            return not self == other

        def __str__(self):
            return 'StringContaining(%s)' % substr

        def __repr__(self):
            return str(self)

    return StringContaining()
